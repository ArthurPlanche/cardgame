package com.sp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Carte {
	
	@Id
	@GeneratedValue
	private Integer id;
	private String name;
	private int HP;
	private int Energy;
	private int attaque;
	private int defense;
	private int prix;
	private String imgUrl;
	private String Description;
	
	public Carte() {
	}

	public Carte(int id,String name, int HP, int Energy, String imgUrl, int attaque, int defense, int prix, String Description) {
		super();
		this.id=id;
		this.name = name;
		this.HP = HP;
		this.Energy = Energy;
		this.imgUrl = imgUrl;
		this.attaque = attaque;
		this.defense = defense;
		this.prix = prix;
		this.Description = Description;
	}

	public int getAttaque() {
		return attaque;
	}

	public void setAttaque(int attaque) {
		this.attaque = attaque;
	}

	public int getDefense() {
		return defense;
	}

	public void setDefense(int defense) {
		this.defense = defense;
	}

	public int getPrix() {
		return prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return Description;
	}

	public void setDescription(String Description) {
		this.Description = Description;
	}

	public int getHP() {
		return HP;
	}

	public void setHPValue(int HP) {
		this.HP = HP;
	}

	public int getEnergy() {
		return Energy;
	}

	public void setEnergyValue(int Energy) {
		this.Energy = Energy;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Card ["+this.id+"]: name:"+this.name+", HP:"+this.HP+", Attaque:"+this.attaque+", Defense:"+this.defense+", Energy:"+this.Energy+", prix:"+this.prix+" imgUrl:"+this.imgUrl+ ", Description:"+this.Description;
	}

}
