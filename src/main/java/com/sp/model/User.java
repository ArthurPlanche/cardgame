package com.sp.model;

import java.util.*;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Users")
public class User {
	@Id
	@GeneratedValue
	private int id;
	private String firstname;
	private String lastname;
	private String username;
	private String passwd;
	private int argent;
	@OneToMany(targetEntity = Carte.class, cascade = CascadeType.ALL)
	@JoinColumn(name= "carte_id", referencedColumnName = "id")
	List<Carte> ListCarte;
	
	public User() {
	}
	
	public User(int id, String FirstName, String LastName,String UserName,String passwd, int argent) {
		super();
		this.id=id;
		this.firstname=FirstName;
		this.lastname=LastName;
		this.username=UserName;
		this.passwd=passwd;
		this.argent=argent;
	}


	
	public String getFirstName() {
		return firstname;
	}


	public void setFirstName(String firstName) {
		this.firstname = firstName;
	}


	public String getLastName() {
		return lastname;
	}


	public void setLastName(String lastName) {
		this.lastname = lastName;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}
	
	public int getNbrCarte() {
		int taille = ListCarte.size();
		return taille;
	}
	
	public List<Carte> getListCarte() {
		return ListCarte;
	}


	public void setListCarte(List<Carte> listCarte) {
		ListCarte = listCarte;
	}

	public String toString() {
		return "User: "+this.id+" prénom: "+this.firstname+" nom: "+this.lastname+" nombre de carte: "/*+getNbrCarte()*/;
	}


	public String getUserName() {
		return username;
	}


	public void setUserName(String userName) {
		username = userName;
	}

	public String getPasswd() {
		return passwd;
	}

	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}



	public int getArgent() {
		return argent;
	}



	public void setArgent(int argent) {
		this.argent = argent;
	}
}