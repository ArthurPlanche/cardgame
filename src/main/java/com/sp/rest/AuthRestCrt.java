package com.sp.rest;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sp.services.AuthService;
import com.sp.services.UserService;

@RestController
public class AuthRestCrt {
	
	@Autowired
    AuthService AService;
	@Autowired
	UserService UService;
	
	@GetMapping("/")
	public void firstPage(HttpServletResponse response) throws IOException {
		UService.CreateUser("Arthur", "Planche", "Marchombre12", "yolo", 1500);
		response.sendRedirect("auth.html");
	}
	
	@RequestMapping(method=RequestMethod.GET,value="/addData")
	public void login(@RequestParam String username, @RequestParam String password, HttpServletResponse response) throws IOException {
		boolean ret = AService.login(username, password);
		if (ret) {
			response.sendRedirect("/cardList.html?idUser="+AService.getId(username));
		}
		else {
			response.sendRedirect("auth.html");
		}
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/newUser/{username}/{pwd}/{firstName}/{lastName}/{money}")
	public boolean newUser(@PathVariable String username, @PathVariable String pwd, @PathVariable String firstName, @PathVariable String lastName, @PathVariable String money) {
		boolean ret = AService.newUser(username, pwd, firstName, lastName, Integer.valueOf(money));
		return ret;
	}

}