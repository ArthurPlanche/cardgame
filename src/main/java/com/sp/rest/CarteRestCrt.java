package com.sp.rest;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sp.model.Carte;
import com.sp.services.CarteService;
import com.sp.services.UserService;

@RestController
@RequestMapping("/card")
public class CarteRestCrt {
	@Autowired
    CarteService cService;
	@Autowired
	UserService uService;
	
	@RequestMapping(method=RequestMethod.GET,value="/{id}")
    public Carte getCard(@PathVariable String id) {
        Carte c=cService.getCarte(Integer.valueOf(id));
        return c;
    }

	@GetMapping("/test3")
    public String test(Model model,@RequestParam int idUser) {
		/*String url =" https://media-exp1.licdn.com/dms/image/C4E03AQGqCwLK6mad1A/profile-displayphoto-shrink_200_200/0/1615303419962?e=1626307200&v=beta&t=lMoo-9eUfARO7k6OFYhWTQSawKFQXWmjVf0HUSmftNI";
		Carte c = new Carte(1,"ThomasBG", 500,1000,url, 500, 500, 1000000, "Thomas, l'homme le plus chaud de ta région");
        cService.addCarte(c);
       
        String url2 ="https://www.pokepedia.fr/images/thumb/6/66/Pingol%C3%A9on-DP.png/1200px-Pingol%C3%A9on-DP.png";
		Carte c2 = new Carte(2,"Pingoleon", 100,500,url2, 75, 100, 250, "Pingoleon est surnomme frequemment l'Empereur des glaces en raison de son apparence royale. S'il est le chef d'une tribu, son trident sera plus gros.");
        cService.addCarte(c2);
        
        List<Carte> listeCarte = cService.getAllCartes();*/
		List<Carte> listeCarte = uService.getCartes(idUser);
        model.addAttribute("listeCarte",listeCarte);
        model.addAttribute("idUser", idUser);
        
        return "cardId";
    }
	

	@GetMapping("/")
	public List<Carte> test3(@RequestParam int idUser, HttpServletResponse response) throws IOException {
		
		List<Carte> listeCarte = uService.getCartes(idUser);
        return listeCarte;
    }
	
	@GetMapping("/test")
    public String test2(Model model) {
		String url =" https://media-exp1.licdn.com/dms/image/C4E03AQGqCwLK6mad1A/profile-displayphoto-shrink_200_200/0/1615303419962?e=1626307200&v=beta&t=lMoo-9eUfARO7k6OFYhWTQSawKFQXWmjVf0HUSmftNI";
		Carte c = new Carte(1,"ThomasBG", 500,1000,url, 500, 500, 1000000, "Thomas, l'homme le plus chaud de ta région");
        cService.addCarte(c);
       
       /* String url2 ="https://www.pokepedia.fr/images/thumb/6/66/Pingol%C3%A9on-DP.png/1200px-Pingol%C3%A9on-DP.png";
		Carte c2 = new Carte(2,"Pingoleon", 100,500,url2, 75, 100, 250, "Pingoleon est surnomme frequemment l'Empereur des glaces en raison de son apparence royale. S'il est le chef d'une tribu, son trident sera plus gros.");
        cService.addCarte(c2);
        
        List<Carte> listeCarte = cService.getAllCartes();
        System.out.println(listeCarte.get(1));
        model.addAttribute("listeCarte",listeCarte);  
        model.addAttribute("size",listeCarte.size());  */
        model.addAttribute("carte",c);
        return "cardIdex";
    }
	
}
