package com.sp.rest;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sp.model.Carte;
import com.sp.services.MarketService;

@Controller
public class MarketRestCrt {
	@Autowired
    MarketService mService;

	@RequestMapping(method=RequestMethod.POST,value="/sellcard/{idcard}/{iduser}") // /sellcard/45/78?prix=150
	public void sellCard(@PathVariable String idcard, @PathVariable String iduser, @RequestParam String prix,HttpServletResponse response) throws IOException { 
		mService.sellCard(Integer.valueOf(idcard), Integer.valueOf(prix), Integer.valueOf(iduser));
		response.sendRedirect("card-full.html");
	}

	@RequestMapping(method=RequestMethod.GET,value="/buy/{idcard}/{idseller}/{idbuyer}")
	public void buyCard(@PathVariable String idcard, @PathVariable String idseller, @PathVariable String idbuyer) {
        mService.buyCard(Integer.valueOf(idcard),Integer.valueOf(idseller),Integer.valueOf(idbuyer));       
    }

	@GetMapping("/testcard")
    public String test(Model model,@RequestParam int idUser) {

		List<Carte> listeCard = mService.getListCard();
        model.addAttribute("listeCarte",listeCard);
        model.addAttribute("idUser", idUser);
        
        return "cardmarketlist";
    }
	
}
