package com.sp.rest;

//import java.util.ArrayList;
//import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sp.model.User;

import com.sp.services.UserService;

@RestController
public class UserRestCrt {
    
    @Autowired
    UserService uService;
    
    @RequestMapping("/hello")
    public String sayHello() {
        return "Hello Hero !!!";
    }
    /*@RequestMapping("/test")
    public void test() {
        User user1 = new User(2,"jacques","Scalise","pedro","present", 10);
        List myArrayList = new ArrayList(); 
        myArrayList.add(1); 
        myArrayList.add(2);
        user1.setListCarte(myArrayList);
        //uService.SupprimerCarte(2,2);
        uService.getArgentService(2);
        System.out.println(user1.getArgent());
    }*/
    
    @RequestMapping(method=RequestMethod.POST,value="/adduser")
    public void addHero(@RequestBody User user) {
        System.out.println(user);
    }

    @RequestMapping(method=RequestMethod.GET,value="/getuser")
    public User getUser(@RequestParam int idUser) {
      User user = uService.findUserId(idUser);
      return user;
    }
}