package com.sp.services;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import com.sp.services.UserService;
import com.sp.model.User;

@Service
public class AuthService {
	
	@Autowired
	UserService uService;
	
	public boolean login (String username, String pwd) {
		boolean ret = false;
		if (uService.findUser(username) != null) {
			String userPwd = uService.findUser(username).getPasswd();
			
			if (userPwd.equals(pwd)) {
				ret = true;
			}
		}
		return ret;
	}
	
	public boolean newUser (String username, String pwd, String firstName, String lastName, int money) {
		boolean ret = false;
		if (uService.CreateUser(username, pwd, firstName, lastName, money)) {
			ret = true;
		}
		return ret;
	}
	
	public int getId(String username) {
		return uService.findUser(username).getId();
	}

}