package com.sp.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.model.Carte;
import com.sp.repository.CarteRepository;

@Service
public class CarteService {
	@Autowired
	CarteRepository cRepository;
	
	public void addCarte(Carte c) {
		Carte createdCarte=cRepository.save(c);
		System.out.println(createdCarte);
	}

	public Carte getCarte(int id) {
		Optional<Carte> cOpt =cRepository.findById(id);
		if (cOpt.isPresent()) {
			return cOpt.get();
		}else {
			return null;
		}
	}
	
	public int getprixCarte(int idCarte) {
		Carte carte = getCarte(idCarte);
		return carte.getPrix();
	}
	
	public void setPrixCarte(int idCarte, int prix) {
		Carte carte = getCarte(idCarte);
		carte.setPrix(prix);
	}
	
	public List<Carte> getAllCartes(){
		Iterable<Carte> listeCarteIterable = cRepository.findAll();
		List<Carte> listeCarte = new ArrayList<Carte>();
		listeCarteIterable.forEach(listeCarte::add);
		return listeCarte;
	}
	
	public Iterable<Carte> getAll() {
		return cRepository.findAll();
	}

}
