package com.sp.services;
import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
//import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sp.model.Carte;
import com.sp.services.CarteService;





@Service
public class MarketService {
	@Autowired
	CarteService cService;
	@Autowired
	UserService uService;
	
//	List<Carte> Cardtosell = new ArrayList<Carte>(); // Liste de Carte à vendre, qu'on affichera sur la page Market
	
	private ArrayList<Carte> ListCard = new ArrayList<Carte>();
	
	public ArrayList<Carte> getListCard() {
        return ListCard;
    }
	
	 public void AddCardToSell(Carte c2) {
	        ListCard.add(c2);
	 }
	 



		
	public void buyCard(int idcard, int idseller, int idbuyer) { 
		
		Carte c1 =cService.getCarte(idcard);
		if (c1 != null) {
			if (uService.getArgentService(idbuyer) >= cService.getprixCarte(idcard)) {// si il a assez d'argent
				uService.SupprimerArgent(idbuyer, cService.getprixCarte(idcard));
				uService.AddCarte(idbuyer, idcard);
				uService.AddArgent(idseller, cService.getprixCarte(idcard));
			}
		}
	}
				


	public void sellCard(int idUser, int idCard, int prix) {
		uService.SupprimerCarte(idUser, idCard);
		cService.setPrixCarte(idCard, prix);
		Carte card_to_sell = cService.getCarte(idCard);
		AddCardToSell(card_to_sell);
		
				

	}
	


}
