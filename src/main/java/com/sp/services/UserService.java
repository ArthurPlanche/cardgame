package com.sp.services;

import java.util.ArrayList;
//import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import com.sp.model.Carte;
import com.sp.model.User;
import com.sp.repository.UserRepository;


@Service

public class UserService {
	
	@Autowired
	UserRepository uRepository;
	@Autowired
	CarteService cService;
	@Autowired
	MarketService mService;

	public User findUser(String username) {
		 Optional<User> user = uRepository.findByUsername(username);
		if (user.isPresent()) {
			return user.get();
		}else {
			return null;
		}
	}
		
	public User findUserId(int idUser){
		Optional<User> user = uRepository.findById(idUser);
		if (user.isPresent()) {
			return user.get();
		}else {
			return null;
		}
	}

	
	public boolean CreateUser(String FirstName, String LastName,String UserName, String passwd,int argent) {
		//test condition si login est vide
		if (UserName.isEmpty()) {
			return false;
		}
		else {
			User user = new User (1,FirstName,LastName, UserName,passwd, argent);
			///////////////////////////////////////
			/*ajout de carte, pas à faire là mais pour test*/
			String url =" https://media-exp1.licdn.com/dms/image/C4E03AQGqCwLK6mad1A/profile-displayphoto-shrink_200_200/0/1615303419962?e=1626307200&v=beta&t=lMoo-9eUfARO7k6OFYhWTQSawKFQXWmjVf0HUSmftNI";
			Carte c = new Carte(1,"ThomasBG", 500,1000,url, 500, 500, 1000000, "Thomas, l'homme le plus chaud de ta région");
	        cService.addCarte(c);
	       
	        String url2 ="https://www.pokepedia.fr/images/thumb/6/66/Pingol%C3%A9on-DP.png/1200px-Pingol%C3%A9on-DP.png";
			Carte c2 = new Carte(2,"Pingoleon", 100,500,url2, 75, 100, 250, "Pingoleon est surnomme frequemment l'Empereur des glaces en raison de son apparence royale. S'il est le chef d'une tribu, son trident sera plus gros.");
	        cService.addCarte(c2);
	        
	        String url3 =" https://pbs.twimg.com/profile_images/1220452744579948544/SXcvaAYp_400x400.jpg";
	        Carte c3 = new Carte(3,"Uchiwa Sasuke", 300,8000,url3, 300, 400, 6666, "Je n'ai qu'un but, tuer un homme");
	        cService.addCarte(c3);
	        mService.AddCardToSell(c3);	        
	        
	        List<Carte> listeCarte = cService.getAllCartes();
	        listeCarte.remove(2);
	        
	        user.setListCarte(listeCarte);
	        ///////////////////////////////////
			uRepository.save(user);
			return true;
		}
	}
	
	public void AddCarte(int idUser, int idCarte) {
		Optional<User> user = uRepository.findById(idUser);
		if(user.isPresent()) {
			// List<Carte> list=user.get().getListCarte();
			List<Carte> list= user.get().getListCarte();
			//list.add(2);
			user.get().setListCarte(list);
		 }
	}
	

  public List<Carte> getCartes(int idUser){
	  return uRepository.findById(idUser)
			  .map(User::getListCarte)
			  .orElseThrow(() -> new RuntimeException(String.format("Pas d'utilisateur avec l'identifiant %d", idUser)));
  }
	
	
	public void SupprimerCarte(int id, int idCarte) {
		Optional<User> user = uRepository.findById(id);
		 if(user.isPresent()) {
			// List<Carte> list=user.get().getListCarte();
			 user.get().getListCarte().remove(idCarte);
			 
		 }
			 
	}
	
	public void AddArgent(int idUser, int Argent) {
		Optional<User> user = uRepository.findById(idUser);
		if(user.isPresent()) {
			int argent=user.get().getArgent();
			int somme = Argent + argent;
			user.get().setArgent(somme);

		}
	}
	public void SupprimerArgent(int idUser, int Argent) {
		Optional<User> user = uRepository.findById(idUser);
		if(user.isPresent()) {
			int argent=user.get().getArgent();
			int difference = Argent - argent;
			user.get().setArgent(difference);

		}
		
		
	}
	
	
	
	public int getArgentService(int id) {
		 Optional<User> user = uRepository.findById(id);
		 if(user.isPresent()) {
			 return user.get().getArgent();
		 }
		 else {
			 return -1;
		 }
	}
	
}