document.addEventListener("DOMContentLoaded", function(event) {
    let template = document.querySelector("#row");
    let idUser = location.search.split("idUser")[1];
    const context = {
        method: 'GET'
    }
    fetch('http://localhost:8081/card/?idUser'+idUser, context)
        .then(response => response.json().then(body => templating(body)))
        .catch(error => console.log(error))
    
        function templating(body){
            
            for(const card of body){
                let clone = document.importNode(template.content, true);
        
                newContent= clone.firstElementChild.innerHTML
                            .replace(/{{url}}/g, card.imgUrl)
                            .replace(/{{name}}/g, card.name)
                            .replace(/{{description}}/g, card.description)
                            .replace(/{{hp}}/g, card.hp)
                            .replace(/{{energy}}/g, card.energy)
                            .replace(/{{attack}}/g, card.attaque)
                            .replace(/{{defense}}/g, card.defense)
                            .replace(/{{prix}}/g, card.prix);
                clone.firstElementChild.innerHTML= newContent;

                let cardContainer= document.querySelector("#tableContent");
                cardContainer.appendChild(clone);
            }
        }
});