document.addEventListener("DOMContentLoaded", function(event) {
    let template = document.querySelector("#user");
    let idUser = location.search.split("idUser")[1];
    const context = {
        method: 'GET'
    }
    fetch('http://localhost:8081/getuser?idUser'+idUser, context)
        .then(response => response.json().then(body => userInfo(body)))
        .catch(error => console.log(error))
    
        function userInfo(body){
            document.querySelector('#userNameId').innerText = body.userName;
            document.querySelector('#money').innerText = body.argent + " $";
        }
});